# Invoice System App Backend Golang

These System built with Gin, Gorm and Mysql for database 
## How To Use
- Download .sql and download golang
- You can use my link [Google Drive] here 

## Installation
Before Install Golang you should extract .zip .rar golang file.
example : /Users/userpc/app/invoice
```sh
cd /Users/userpc/app/invoice
go install
```

For setting .env file (connect database mysql)
these example for local database mysql
```sh
DB_USERNAME=root
DB_PASSWORD=
DB_HOST=localhost
DB_PORT=3306
DB_NAME=invoice_app 
```
## How To Run Golang Program
- Run these command 
 ```sh
go run main.go
```
- As you can see golang running HTTP on :8080

 ```sh
http://localhost:8080/
```

## Postman

Testing API Golang 
You can Running postman files by import my postman collection .json file from [Google Drive]
- Create new data invoice (POST)
to create new data invoice 
detail_item_json data detail item from frontend with json format and then golang will unmarshal (save into database table 'detail_items')
 ```sh
 http://localhost:8080/api/create-invoice
``` 
- Delete (DELETE)
delete detail item. example id = 1
 ```sh
http://localhost:8080/api/detail-items/${id}
``` 
- Update (PUT)
update data invoice and detail invoice for example id = 1
 ```sh
http://localhost:8080/api/update-invoice/${id}
``` 
- Get id invoice (GET)
get id data invoice and detail invoices for example id = 1
 ```sh
 http://localhost:8080/api/invoice/${id}
``` 
- Get data (GET)
get all data invoice 
 ```sh
 http://localhost:8080/api/invoice
``` 
- Calculate (POST) 
used for instant calculate when user while typing in form text detail item before submit/save/update
only used for calculate: "sub_total", "tax", "grand_total", detail items ("amount")
 ```sh
http://localhost:8080/api/calculate 
``` 

- Indexing (GET)
Indexing search/filter using query param
for example:
1. issued_date = 2022-10-22
2. due_date = 2023-03-18
 ```sh
 http://localhost:8080/api/invoice-indexing?issued_date=2022-10-22&due_date=2023-03-18
``` 
 
   [Google Drive]: <https://drive.google.com/drive/folders/1cJHm7HRJuTanmgc5rdNWtJejWsLh1yiV?usp=share_link>
   

   
